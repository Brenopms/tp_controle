class Gain
  Modelica.Electrical.Analog.Ideal.IdealOpAmp3Pin idealOpAmp3Pin1 annotation(
    Placement(visible = true, transformation(origin = {22, 16}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Resistor resistor1(R = 1000)  annotation(
    Placement(visible = true, transformation(origin = {18, 52}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Resistor resistor2(R = 1000)  annotation(
    Placement(visible = true, transformation(origin = {-20, 22}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Interfaces.Pin Input_pin annotation(
    Placement(visible = true, transformation(origin = {-74, 22}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-74, 22}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Interfaces.Pin Output_pin annotation(
    Placement(visible = true, transformation(origin = {208, 16}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {94, 24}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Ground ground1 annotation(
    Placement(visible = true, transformation(origin = {-8, -12}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Ground ground2 annotation(
    Placement(visible = true, transformation(origin = {132, -14}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Resistor resistor3(R = 1000) annotation(
    Placement(visible = true, transformation(origin = {116, 20}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Ideal.IdealOpAmp3Pin idealOpAmp3Pin2 annotation(
    Placement(visible = true, transformation(origin = {158, 16}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Resistor resistor4(R = 1000) annotation(
    Placement(visible = true, transformation(origin = {158, 50}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Ideal.IdealOpAmp3Pin idealOpAmp3Pin3 annotation(
    Placement(visible = true, transformation(origin = {76, 20}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(idealOpAmp3Pin3.out, resistor3.p) annotation(
    Line(points = {{86, 20}, {106, 20}, {106, 20}, {106, 20}}, color = {0, 0, 255}));
  connect(idealOpAmp3Pin3.in_p, idealOpAmp3Pin1.out) annotation(
    Line(points = {{66, 15}, {53, 15}, {53, 17}, {40, 17}, {40, 17}, {32, 17}, {32, 15}}, color = {0, 0, 255}));
  connect(idealOpAmp3Pin3.in_n, idealOpAmp3Pin3.out) annotation(
    Line(points = {{66, 25}, {60, 25}, {60, 27}, {54, 27}, {54, 47}, {96, 47}, {96, 21}, {86, 21}, {86, 21}, {86, 21}, {86, 19}, {86, 19}}, color = {0, 0, 255}));
  connect(resistor4.n, idealOpAmp3Pin2.out) annotation(
    Line(points = {{168, 50}, {176, 50}, {176, 50}, {182, 50}, {182, 16}, {168, 16}, {168, 16}}, color = {0, 0, 255}));
  connect(resistor4.p, resistor3.n) annotation(
    Line(points = {{148, 50}, {143, 50}, {143, 50}, {136, 50}, {136, 20}, {126, 20}, {126, 20}, {126, 20}, {126, 20}, {126, 20}}, color = {0, 0, 255}));
  connect(Output_pin, idealOpAmp3Pin2.out) annotation(
    Line(points = {{208, 16}, {180, 16}, {180, 16}, {168, 16}, {168, 16}, {168, 16}, {168, 16}, {168, 16}, {168, 16}}, color = {0, 0, 255}));
  connect(ground2.p, idealOpAmp3Pin2.in_p) annotation(
    Line(points = {{132, -4}, {132, -4}, {132, 10}, {148, 10}, {148, 12}}, color = {0, 0, 255}));
  connect(resistor3.n, idealOpAmp3Pin2.in_n) annotation(
    Line(points = {{126, 20}, {138, 20}, {138, 20}, {148, 20}, {148, 22}, {149, 22}, {149, 22}, {148, 22}}, color = {0, 0, 255}));
  connect(ground1.p, idealOpAmp3Pin1.in_p) annotation(
    Line(points = {{-8, -2}, {-8, 4.5}, {-8, 4.5}, {-8, 11}, {12, 11}}, color = {0, 0, 255}));
  connect(resistor1.p, resistor2.n) annotation(
    Line(points = {{8, 52}, {2, 52}, {2, 22}, {-10, 22}, {-10, 22}, {-10, 22}, {-10, 22}, {-10, 22}, {-10, 22}}, color = {0, 0, 255}));
  connect(Input_pin, resistor2.p) annotation(
    Line(points = {{-74, 22}, {-30, 22}}, color = {0, 0, 255}));
  connect(resistor2.n, idealOpAmp3Pin1.in_n) annotation(
    Line(points = {{-10, 22}, {12, 22}, {12, 21}}, color = {0, 0, 255}));
  connect(resistor1.n, idealOpAmp3Pin1.out) annotation(
    Line(points = {{28, 52}, {40, 52}, {40, 16}, {32, 16}}, color = {0, 0, 255}));
  annotation(
    uses(Modelica(version = "3.2.2")),
    Diagram(coordinateSystem(extent = {{-100, -100}, {250, 100}})),
    Icon(coordinateSystem(extent = {{-100, -100}, {250, 100}})),
    version = "",
    __OpenModelica_commandLineOptions = "");
end Gain;
