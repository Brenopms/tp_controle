class RC
  Modelica.Electrical.Analog.Basic.Resistor resistor1(R = 10000) annotation(
    Placement(visible = true, transformation(origin = {28, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Capacitor capacitor1(C = 0.0001) annotation(
    Placement(visible = true, transformation(origin = {58, -10}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Modelica.Electrical.Analog.Ideal.IdealOpAmpLimited idealOpAmpLimited1 annotation(
    Placement(visible = true, transformation(origin = {-4, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Ground ground1 annotation(
    Placement(visible = true, transformation(origin = {0, -74}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Sources.ConstantVoltage constantVoltage1(V = 5) annotation(
    Placement(visible = true, transformation(origin = {-4, 36}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
  Modelica.Electrical.Analog.Sources.ConstantVoltage constantVoltage2(V = 5) annotation(
    Placement(visible = true, transformation(origin = {-4, -30}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
  Modelica.Electrical.Analog.Basic.Ground ground2 annotation(
    Placement(visible = true, transformation(origin = {-4, 68}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
  Modelica.Electrical.Analog.Basic.Ground ground3 annotation(
    Placement(visible = true, transformation(origin = {58, -38}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Interfaces.Pin inputcelula annotation(
    Placement(visible = true, transformation(origin = {-38, -6}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Interfaces.Pin outputcelula annotation(
    Placement(visible = true, transformation(origin = {90, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(constantVoltage2.p, ground1.p) annotation(
    Line(points = {{-4, -40}, {0, -40}, {0, -64}, {0, -64}}, color = {0, 0, 255}));
  connect(idealOpAmpLimited1.in_p, inputcelula) annotation(
    Line(points = {{-14, -6}, {-38, -6}}, color = {0, 0, 255}));
  connect(capacitor1.p, outputcelula) annotation(
    Line(points = {{58, 0}, {92, 0}, {92, 0}, {90, 0}}, color = {0, 0, 255}));
  connect(idealOpAmpLimited1.in_n, resistor1.p) annotation(
    Line(points = {{-14, 6}, {-30, 6}, {-30, 20}, {16, 20}, {16, 0}, {18, 0}, {18, 0}}, color = {0, 0, 255}));
  connect(capacitor1.n, ground3.p) annotation(
    Line(points = {{58, -20}, {58, -20}, {58, -28}, {58, -28}}, color = {0, 0, 255}));
  connect(constantVoltage2.p, ground1.p) annotation(
    Line(points = {{-4, -40}, {-4, -40}, {-4, -48}, {-4, -48}}, color = {0, 0, 255}));
  connect(idealOpAmpLimited1.VMin, constantVoltage2.n) annotation(
    Line(points = {{-4, -8}, {-4, -8}, {-4, -20}, {-4, -20}}, color = {0, 0, 255}));
  connect(constantVoltage1.p, idealOpAmpLimited1.VMax) annotation(
    Line(points = {{-4, 26}, {-4, 26}, {-4, 8}, {-4, 8}}, color = {0, 0, 255}));
  connect(ground2.p, constantVoltage1.n) annotation(
    Line(points = {{-4, 58}, {-4, 46}}, color = {0, 0, 255}));
  connect(resistor1.n, capacitor1.p) annotation(
    Line(points = {{38, 0}, {58, 0}, {58, 0}, {58, 0}}, color = {0, 0, 255}));
  connect(idealOpAmpLimited1.out, resistor1.p) annotation(
    Line(points = {{6, 0}, {18, 0}, {18, 0}, {18, 0}}, color = {0, 0, 255}));
  annotation(
    uses(Modelica(version = "3.2.2")));
end RC;
