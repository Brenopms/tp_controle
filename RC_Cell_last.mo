class RC_Cell_last
  Modelica.Electrical.Spice3.Basic.Ground ground1 annotation(
    Placement(visible = true, transformation(origin = {-50, -50}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Spice3.Basic.Ground ground2 annotation(
    Placement(visible = true, transformation(origin = {-48, 70}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
  Modelica.Electrical.Spice3.Basic.Ground ground3 annotation(
    Placement(visible = true, transformation(origin = {36, -48}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Sources.ConstantVoltage constantVoltage1(V = 5) annotation(
    Placement(visible = true, transformation(origin = {-48, 42}, extent = {{-10, -10}, {10, 10}}, rotation = 90)));
  Modelica.Electrical.Analog.Sources.ConstantVoltage constantVoltage2(V = 5) annotation(
    Placement(visible = true, transformation(origin = {-50, -14}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Modelica.Electrical.Analog.Basic.Resistor resistor1(R = 10000) annotation(
    Placement(visible = true, transformation(origin = {-4, 14}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Capacitor capacitor1(C = 0.0001) annotation(
    Placement(visible = true, transformation(origin = {36, -14}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Modelica.Electrical.Analog.Ideal.IdealOpAmpLimited idealOpAmpLimited1 annotation(
    Placement(visible = true, transformation(origin = {-48, 14}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Interfaces.Pin Input_PIN annotation(
    Placement(visible = true, transformation(origin = {-80, 8}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-80, 8}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(idealOpAmpLimited1.in_p, Input_PIN) annotation(
    Line(points = {{-58, 8}, {-80, 8}, {-80, 8}, {-80, 8}}, color = {0, 0, 255}));
  connect(idealOpAmpLimited1.in_n, idealOpAmpLimited1.out) annotation(
    Line(points = {{-58, 20}, {-76, 20}, {-76, 26}, {-38, 26}, {-38, 14}, {-38, 14}}, color = {0, 0, 255}));
  connect(resistor1.p, idealOpAmpLimited1.out) annotation(
    Line(points = {{-14, 14}, {-38, 14}, {-38, 14}, {-38, 14}}, color = {0, 0, 255}));
  connect(constantVoltage1.p, idealOpAmpLimited1.VMax) annotation(
    Line(points = {{-48, 32}, {-48, 32}, {-48, 22}, {-48, 22}}, color = {0, 0, 255}));
  connect(idealOpAmpLimited1.VMin, constantVoltage2.p) annotation(
    Line(points = {{-48, 6}, {-50, 6}, {-50, -4}, {-50, -4}}, color = {0, 0, 255}));
  connect(capacitor1.n, ground3.p) annotation(
    Line(points = {{36, -24}, {36, -24}, {36, -38}, {36, -38}}, color = {0, 0, 255}));
  connect(resistor1.n, capacitor1.p) annotation(
    Line(points = {{6, 14}, {36, 14}, {36, -4}, {36, -4}, {36, -4}}, color = {0, 0, 255}));
  connect(ground2.p, constantVoltage1.n) annotation(
    Line(points = {{-48, 60}, {-48, 60}, {-48, 52}, {-48, 52}}, color = {0, 0, 255}));
  connect(constantVoltage2.n, ground1.p) annotation(
    Line(points = {{-50, -24}, {-50, -24}, {-50, -40}, {-50, -40}}, color = {0, 0, 255}));
  annotation(
    uses(Modelica(version = "3.2.2")),
    Diagram);
end RC_Cell_last;
