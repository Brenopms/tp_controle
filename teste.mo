model teste
  Modelica.Electrical.Analog.Basic.Ground ground1 annotation(
    Placement(visible = true, transformation(origin = {-80, 4}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Ground ground2 annotation(
    Placement(visible = true, transformation(origin = {62, 20}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Sources.StepVoltage stepVoltage1 annotation(
    Placement(visible = true, transformation(origin = {-60, 32}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
  Gain gain1 annotation(
    Placement(visible = true, transformation(origin = {0, 32}, extent = {{-10, -10}, {25, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Resistor resistor1(R = 1)  annotation(
    Placement(visible = true, transformation(origin = {44, 34}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(resistor1.n, ground2.p) annotation(
    Line(points = {{54, 34}, {62, 34}, {62, 30}, {62, 30}, {62, 30}}, color = {0, 0, 255}));
  connect(gain1.Output_pin, resistor1.p) annotation(
    Line(points = {{20, 34}, {34, 34}, {34, 34}, {34, 34}}, color = {0, 0, 255}));
  connect(stepVoltage1.p, gain1.Input_pin) annotation(
    Line(points = {{-50, 32}, {-6, 32}, {-6, 34}, {-8, 34}}, color = {0, 0, 255}));
  connect(ground1.p, stepVoltage1.n) annotation(
    Line(points = {{-80, 14}, {-80, 14}, {-80, 32}, {-70, 32}, {-70, 32}}, color = {0, 0, 255}));
  annotation(
    uses(Modelica(version = "3.2.2")));
end teste;
