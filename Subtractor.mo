class Subtractor
  Modelica.Electrical.Analog.Basic.Resistor resistor1(R = 10000)  annotation(
    Placement(visible = true, transformation(origin = {-20, -2}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Resistor resistor2(R = 10000)  annotation(
    Placement(visible = true, transformation(origin = {-18, -20}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Resistor resistor3(R = 10000)  annotation(
    Placement(visible = true, transformation(origin = {26, 28}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Resistor resistor4(R = 10000)  annotation(
    Placement(visible = true, transformation(origin = {8, -48}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Modelica.Electrical.Analog.Basic.Ground ground1 annotation(
    Placement(visible = true, transformation(origin = {8, -80}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Interfaces.Pin Output annotation(
    Placement(visible = true, transformation(origin = {84, -8}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {84, 28}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Ideal.IdealOpAmp3Pin idealOpAmp3Pin1 annotation(
    Placement(visible = true, transformation(origin = {46, -8}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Interfaces.Pin Positive_pin annotation(
    Placement(visible = true, transformation(origin = {-76, 28}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-76, 28}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Interfaces.Pin Negative_pin1 annotation(
    Placement(visible = true, transformation(origin = {-76, -38}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {8, -66}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(idealOpAmp3Pin1.in_p, resistor2.n) annotation(
    Line(points = {{36, -13}, {12, -13}, {12, -20}, {-8, -20}}, color = {0, 0, 255}));
  connect(idealOpAmp3Pin1.in_n, resistor1.n) annotation(
    Line(points = {{36, -3}, {20, -3}, {20, -2}, {-10, -2}}, color = {0, 0, 255}));
  connect(resistor3.n, idealOpAmp3Pin1.out) annotation(
    Line(points = {{36, 28}, {36, 27.5}, {36, 27.5}, {36, 27}, {56, 27}, {56, 9.5}, {56, 9.5}, {56, -8}}, color = {0, 0, 255}));
  connect(idealOpAmp3Pin1.out, Output) annotation(
    Line(points = {{56, -8}, {82, -8}, {82, -8}, {84, -8}}, color = {0, 0, 255}));
  connect(resistor4.n, ground1.p) annotation(
    Line(points = {{8, -58}, {8, -70}}, color = {0, 0, 255}));
  connect(resistor4.p, resistor2.n) annotation(
    Line(points = {{8, -38}, {8, -38}, {8, -20}, {-8, -20}, {-8, -20}}, color = {0, 0, 255}));
  connect(resistor3.p, resistor1.n) annotation(
    Line(points = {{16, 28}, {2, 28}, {2, -2}, {-10, -2}}, color = {0, 0, 255}));
  connect(Positive_pin, resistor2.p) annotation(
    Line(points = {{-76, 28}, {-43, 28}, {-43, -20}, {-28, -20}}, color = {0, 0, 255}));
  connect(Negative_pin1, resistor1.p) annotation(
    Line(points = {{-76, -38}, {-52, -38}, {-52, -2}, {-30, -2}}, color = {0, 0, 255}));
  annotation(
    uses(Modelica(version = "3.2.2")));
end Subtractor;
