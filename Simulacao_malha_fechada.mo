model Simulacao_malha_fechada
  Modelica.Electrical.Analog.Basic.Ground ground1 annotation(
    Placement(visible = true, transformation(origin = {-94, -14}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Sources.StepVoltage stepVoltage1(V = 1)  annotation(
    Placement(visible = true, transformation(origin = {-72, 14}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
  subtractor1 subtractor11 annotation(
    Placement(visible = true, transformation(origin = {-36, 14}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  RC_Cell rC_Cell1 annotation(
    Placement(visible = true, transformation(origin = {30, 14}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  RC_Cell rC_Cell2 annotation(
    Placement(visible = true, transformation(origin = {68, 14}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  RC_Cell rC_Cell3 annotation(
    Placement(visible = true, transformation(origin = {102, 14}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Gain gain annotation(
    Placement(visible = true, transformation(origin = {-2, 12}, extent = {{-10, -10}, {25, 10}}, rotation = 0)));
  RC_Cell rC_Cell4 annotation(
    Placement(visible = true, transformation(origin = {132, 14}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  RC_Cell rC_Cell5 annotation(
    Placement(visible = true, transformation(origin = {158, 14}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  RC_Cell rC_Cell6 annotation(
    Placement(visible = true, transformation(origin = {186, 14}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  RC_Cell rC_Cell7 annotation(
    Placement(visible = true, transformation(origin = {216, 14}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  RC_Cell rC_Cell8 annotation(
    Placement(visible = true, transformation(origin = {256, 16}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(subtractor11.Sub_out, gain.Input_pin) annotation(
    Line(points = {{-25, 14}, {-10, 14}}, color = {0, 0, 255}));
  connect(rC_Cell8.Output_PIN, subtractor11.Negative_input) annotation(
    Line(points = {{263, 17}, {292, 17}, {292, -16}, {-36, -16}, {-36, 3}}, color = {0, 0, 255}));
  connect(stepVoltage1.p, subtractor11.Positive_input) annotation(
    Line(points = {{-62, 14}, {-47, 14}}, color = {0, 0, 255}));
  connect(rC_Cell7.Output_PIN, rC_Cell8.Input_PIN) annotation(
    Line(points = {{223, 15}, {257.5, 15}, {257.5, 17}, {248, 17}}, color = {0, 0, 255}));
  connect(rC_Cell6.Output_PIN, rC_Cell7.Input_PIN) annotation(
    Line(points = {{193, 15}, {208, 15}}, color = {0, 0, 255}));
  connect(rC_Cell5.Output_PIN, rC_Cell6.Input_PIN) annotation(
    Line(points = {{165, 15}, {178, 15}}, color = {0, 0, 255}));
  connect(rC_Cell4.Output_PIN, rC_Cell5.Input_PIN) annotation(
    Line(points = {{139, 15}, {150, 15}}, color = {0, 0, 255}));
  connect(rC_Cell3.Output_PIN, rC_Cell4.Input_PIN) annotation(
    Line(points = {{110, 16}, {124, 16}, {124, 15}}, color = {0, 0, 255}));
  connect(gain.Output_pin, rC_Cell1.Input_PIN) annotation(
    Line(points = {{7, 14}, {22, 14}}, color = {0, 0, 255}));
  connect(rC_Cell2.Output_PIN, rC_Cell3.Input_PIN) annotation(
    Line(points = {{76, 16}, {94, 16}, {94, 14}, {94, 14}}, color = {0, 0, 255}));
  connect(rC_Cell1.Output_PIN, rC_Cell2.Input_PIN) annotation(
    Line(points = {{38, 16}, {60, 16}, {60, 14}, {60, 14}}, color = {0, 0, 255}));
  connect(stepVoltage1.n, ground1.p) annotation(
    Line(points = {{-82, 14}, {-96, 14}, {-96, -4}, {-94, -4}, {-94, -4}, {-94, -4}, {-94, -4}}, color = {0, 0, 255}));
  annotation(
    uses(Modelica(version = "3.2.2")),
    Diagram(coordinateSystem(extent = {{-100, -100}, {300, 100}})),
    Icon(coordinateSystem(extent = {{-100, -100}, {300, 100}})),
    version = "",
    __OpenModelica_commandLineOptions = "");
end Simulacao_malha_fechada;
