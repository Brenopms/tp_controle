model Simulacao_modelo_malha_fechada
  Modelica.Blocks.Continuous.FirstOrder firstOrder1(T = 1, k = 1.88)  annotation(
    Placement(visible = true, transformation(origin = {4, 26}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Math.Feedback feedback1 annotation(
    Placement(visible = true, transformation(origin = {-42, 26}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Sources.Step step1(height = 1)  annotation(
    Placement(visible = true, transformation(origin = {-78, 26}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Continuous.FirstOrder firstOrder2(T = 1) annotation(
    Placement(visible = true, transformation(origin = {38, 26}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Continuous.FirstOrder firstOrder3(T = 1) annotation(
    Placement(visible = true, transformation(origin = {74, 26}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Continuous.FirstOrder firstOrder4(T = 1) annotation(
    Placement(visible = true, transformation(origin = {112, 26}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Continuous.FirstOrder firstOrder5(T = 1) annotation(
    Placement(visible = true, transformation(origin = {148, 26}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Continuous.FirstOrder firstOrder6(T = 1) annotation(
    Placement(visible = true, transformation(origin = {182, 26}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Continuous.FirstOrder firstOrder7(T = 1) annotation(
    Placement(visible = true, transformation(origin = {222, 26}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Continuous.FirstOrder firstOrder8(T = 1) annotation(
    Placement(visible = true, transformation(origin = {254, 26}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(firstOrder6.y, firstOrder7.u) annotation(
    Line(points = {{193, 26}, {210, 26}}, color = {0, 0, 127}));
  connect(firstOrder7.y, firstOrder8.u) annotation(
    Line(points = {{233, 26}, {242, 26}}, color = {0, 0, 127}));
  connect(firstOrder8.y, feedback1.u2) annotation(
    Line(points = {{265, 26}, {284, 26}, {284, -62}, {-40, -62}, {-40, -22}, {-42, -22}, {-42, 18}}, color = {0, 0, 127}));
  connect(firstOrder5.y, firstOrder6.u) annotation(
    Line(points = {{159, 26}, {170, 26}}, color = {0, 0, 127}));
  connect(firstOrder4.y, firstOrder5.u) annotation(
    Line(points = {{123, 26}, {136, 26}}, color = {0, 0, 127}));
  connect(firstOrder3.y, firstOrder4.u) annotation(
    Line(points = {{85, 26}, {100, 26}}, color = {0, 0, 127}));
  connect(firstOrder2.y, firstOrder3.u) annotation(
    Line(points = {{49, 26}, {62, 26}}, color = {0, 0, 127}));
  connect(firstOrder1.y, firstOrder2.u) annotation(
    Line(points = {{15, 26}, {26, 26}}, color = {0, 0, 127}));
  connect(step1.y, feedback1.u1) annotation(
    Line(points = {{-67, 26}, {-56, 26}, {-56, 26}, {-51, 26}, {-51, 26}, {-47, 26}, {-47, 26}, {-51, 26}}, color = {0, 0, 127}));
  connect(feedback1.y, firstOrder1.u) annotation(
    Line(points = {{-33, 26}, {-9, 26}}, color = {0, 0, 127}));
  annotation(
    uses(Modelica(version = "3.2.2")),
    Diagram(coordinateSystem(extent = {{-100, -100}, {300, 100}})),
    Icon(coordinateSystem(extent = {{-100, -100}, {300, 100}})),
    version = "",
    __OpenModelica_commandLineOptions = "");
end Simulacao_modelo_malha_fechada;
