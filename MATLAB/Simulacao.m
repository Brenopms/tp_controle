load("workspace_trabalho_final") %Importar dados do workspace

%-------------- Funcao de Transferencia da malha --------------------------

G = tf(1, [1 8 28 56 70 56 28 8 1])
figure(1)
subplot(2,1,1)
step(G)
subplot(2,1,2)
bode(G)
G_info = stepinfo(G)

%-------------- Malha Fechada Nao Compensada --------------------------

figure(2)
subplot(2,2,1)
nyquist(malha_fechada)
subplot(2,2,2)
rlocus(malha_fechada)
subplot(2,2,[3,4])
step(malha_fechada)
figure(3)
bode(malha_fechada)
malha_fechada_info = stepinfo(malha_fechada)

%-------------- Malha Fechada Compensada --------------------------

figure(4)
title('Line Plot 2')
subplot(2,2,1)
nyquist(malha_fechada_compensada)
subplot(2,2,2)
rlocus(malha_fechada_compensada)
subplot(2,2,[3,4])
step(malha_fechada_compensada)
hold on
step(malha_fechada)
legend('Malha Compensada','Malha nao Compensada')
figure(5)
bode(malha_fechada_compensada)
hold on
bode(malha_fechada)
legend('Malha Compensada','Malha nao Compensada')
malha_fechada_compensada_info = stepinfo(malha_fechada_compensada)
