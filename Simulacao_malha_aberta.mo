model Simulacao_malha_aberta
  Modelica.Electrical.Analog.Sources.StepVoltage stepVoltage1(V = -20)  annotation(
    Placement(visible = true, transformation(origin = {-56, 14}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
  Modelica.Electrical.Analog.Basic.Ground ground2 annotation(
    Placement(visible = true, transformation(origin = {-72, -4}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  RC rc1 annotation(
    Placement(visible = true, transformation(origin = {-26, 14}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  RC rc2 annotation(
    Placement(visible = true, transformation(origin = {2, 14}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  RC rc3 annotation(
    Placement(visible = true, transformation(origin = {32, 14}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  RC rc4 annotation(
    Placement(visible = true, transformation(origin = {60, 14}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  RC rc5 annotation(
    Placement(visible = true, transformation(origin = {88, 14}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  RC rc6 annotation(
    Placement(visible = true, transformation(origin = {116, 14}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  RC rc7 annotation(
    Placement(visible = true, transformation(origin = {144, 14}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  RC_Cell_last rC_Cell_last1 annotation(
    Placement(visible = true, transformation(origin = {168, 14}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(rc7.outputcelula, rC_Cell_last1.Input_PIN) annotation(
    Line(points = {{155, 14}, {158, 14}, {158, 15}, {159.5, 15}, {159.5, 15}, {159.25, 15}, {159.25, 15}, {159, 15}}, color = {0, 0, 255}));
  connect(rc6.outputcelula, rc7.inputcelula) annotation(
    Line(points = {{127, 14}, {130, 14}, {130, 14}, {131, 14}, {131, 14}, {133, 14}}, color = {0, 0, 255}));
  connect(rc5.outputcelula, rc6.inputcelula) annotation(
    Line(points = {{99, 14}, {102, 14}, {102, 14}, {105, 14}, {105, 14}, {105, 14}}, color = {0, 0, 255}));
  connect(rc4.outputcelula, rc5.inputcelula) annotation(
    Line(points = {{71, 14}, {75, 14}, {75, 14}, {77, 14}}, color = {0, 0, 255}));
  connect(rc3.outputcelula, rc4.inputcelula) annotation(
    Line(points = {{43, 14}, {49, 14}, {49, 14}, {49, 14}}, color = {0, 0, 255}));
  connect(rc2.outputcelula, rc3.inputcelula) annotation(
    Line(points = {{13, 14}, {16, 14}, {16, 14}, {19, 14}, {19, 14}, {21, 14}}, color = {0, 0, 255}));
  connect(rc1.outputcelula, rc2.inputcelula) annotation(
    Line(points = {{-15, 14}, {-13, 14}, {-13, 14}, {-11, 14}, {-11, 14}, {-9, 14}, {-9, 14}, {-9, 14}, {-9, 14}, {-9, 14}}, color = {0, 0, 255}));
  connect(stepVoltage1.p, rc1.inputcelula) annotation(
    Line(points = {{-46, 14}, {-41, 14}, {-41, 14}, {-36, 14}, {-36, 14}, {-37, 14}, {-37, 14}, {-36.5, 14}, {-36.5, 14}, {-36, 14}}, color = {0, 0, 255}));
  connect(ground2.p, stepVoltage1.n) annotation(
    Line(points = {{-72, 6}, {-72.5, 6}, {-72.5, 6}, {-73, 6}, {-73, 6}, {-72, 6}, {-72, 14}, {-66, 14}, {-66, 14}, {-66, 14}, {-66, 13}, {-66, 13}, {-66, 14}}, color = {0, 0, 255}));
  annotation(
    uses(Modelica(version = "3.2.2")),
    Diagram(coordinateSystem(extent = {{-100, -100}, {200, 100}})),
    Icon(coordinateSystem(extent = {{-100, -100}, {200, 100}})),
    version = "",
    __OpenModelica_commandLineOptions = "");end Simulacao_malha_aberta;
