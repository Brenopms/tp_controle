class subtractor1
  Modelica.Electrical.Analog.Ideal.IdealOpAmp3Pin idealOpAmp3Pin1 annotation(
    Placement(visible = true, transformation(origin = {0, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Resistor resistor1(R = 1000) annotation(
    Placement(visible = true, transformation(origin = {-38, 4}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Resistor resistor2(R = 1000) annotation(
    Placement(visible = true, transformation(origin = {-60, -6}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Resistor resistor3(R = 1000) annotation(
    Placement(visible = true, transformation(origin = {-22, -32}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Modelica.Electrical.Analog.Basic.Resistor resistor4(R = 1000) annotation(
    Placement(visible = true, transformation(origin = {0, 34}, extent = {{-10, -10}, {10, 10}}, rotation = 180)));
  Modelica.Electrical.Analog.Basic.Ground ground1 annotation(
    Placement(visible = true, transformation(origin = {-22, -56}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Interfaces.Pin Negative_input annotation(
    Placement(visible = true, transformation(origin = {-90, 18}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {0, -110}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Interfaces.Pin Positive_input annotation(
    Placement(visible = true, transformation(origin = {-86, -10}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Interfaces.Pin Sub_out annotation(
    Placement(visible = true, transformation(origin = {80, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(resistor1.n, idealOpAmp3Pin1.in_n) annotation(
    Line(points = {{-28, 4}, {-10, 4}, {-10, 5}}, color = {0, 0, 255}));
  connect(idealOpAmp3Pin1.in_p, resistor3.p) annotation(
    Line(points = {{-10, -5}, {-22, -5}, {-22, -22}}, color = {0, 0, 255}));
  connect(resistor2.n, idealOpAmp3Pin1.in_p) annotation(
    Line(points = {{-50, -6}, {-29, -6}, {-29, -5}, {-10, -5}}, color = {0, 0, 255}));
  connect(resistor4.n, idealOpAmp3Pin1.in_n) annotation(
    Line(points = {{-10, 34}, {-18, 34}, {-18, 4}, {-10, 4}, {-10, 5}}, color = {0, 0, 255}));
  connect(resistor4.p, idealOpAmp3Pin1.out) annotation(
    Line(points = {{10, 34}, {10, 0}}, color = {0, 0, 255}));
  connect(idealOpAmp3Pin1.out, Sub_out) annotation(
    Line(points = {{10, 0}, {80, 0}}, color = {0, 0, 255}));
  connect(resistor2.p, Positive_input) annotation(
    Line(points = {{-70, -6}, {-75, -6}, {-75, -10}, {-86, -10}}, color = {0, 0, 255}));
  connect(resistor1.p, Negative_input) annotation(
    Line(points = {{-48, 4}, {-71, 4}, {-71, 18}, {-90, 18}}, color = {0, 0, 255}));
  connect(resistor3.n, ground1.p) annotation(
    Line(points = {{-22, -42}, {-22, -42}, {-22, -46}, {-22, -46}}, color = {0, 0, 255}));
  annotation(
    uses(Modelica(version = "3.2.2")));
end subtractor1;
