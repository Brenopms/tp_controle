model Simulacao_modelo_malha_aberta
  Modelica.Blocks.Continuous.FirstOrder firstOrder1(T = 1, k = 1) annotation(
    Placement(visible = true, transformation(origin = {-36, 18}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Sources.Step step1(height = 1) annotation(
    Placement(visible = true, transformation(origin = {-70, 18}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Continuous.FirstOrder firstOrder2(T = 1) annotation(
    Placement(visible = true, transformation(origin = {-4, 18}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Continuous.FirstOrder firstOrder3(T = 1) annotation(
    Placement(visible = true, transformation(origin = {32, 18}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Continuous.FirstOrder firstOrder4(T = 1) annotation(
    Placement(visible = true, transformation(origin = {70, 18}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Continuous.FirstOrder firstOrder5(T = 1) annotation(
    Placement(visible = true, transformation(origin = {104, 18}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Continuous.FirstOrder firstOrder6(T = 1) annotation(
    Placement(visible = true, transformation(origin = {136, 16}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Continuous.FirstOrder firstOrder7(T = 1) annotation(
    Placement(visible = true, transformation(origin = {166, 16}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Continuous.FirstOrder firstOrder8(T = 1) annotation(
    Placement(visible = true, transformation(origin = {196, 16}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Interfaces.RealOutput y annotation(
    Placement(visible = true, transformation(origin = {226, 16}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {226, 16}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(firstOrder8.y, y) annotation(
    Line(points = {{207, 16}, {219, 16}, {219, 16}, {225, 16}}, color = {0, 0, 127}));
  connect(firstOrder7.y, firstOrder8.u) annotation(
    Line(points = {{177, 16}, {184, 16}}, color = {0, 0, 127}));
  connect(firstOrder6.y, firstOrder7.u) annotation(
    Line(points = {{147, 16}, {154, 16}}, color = {0, 0, 127}));
  connect(firstOrder5.y, firstOrder6.u) annotation(
    Line(points = {{115, 18}, {120, 18}, {120, 18}, {125, 18}, {125, 16}, {124, 16}, {124, 16}, {122.5, 16}, {122.5, 16}, {123, 16}}, color = {0, 0, 127}));
  connect(firstOrder4.y, firstOrder5.u) annotation(
    Line(points = {{81, 18}, {92, 18}}, color = {0, 0, 127}));
  connect(firstOrder3.y, firstOrder4.u) annotation(
    Line(points = {{43, 18}, {58, 18}}, color = {0, 0, 127}));
  connect(firstOrder2.y, firstOrder3.u) annotation(
    Line(points = {{7, 18}, {20, 18}}, color = {0, 0, 127}));
  connect(firstOrder1.y, firstOrder2.u) annotation(
    Line(points = {{-25, 18}, {-16, 18}}, color = {0, 0, 127}));
  connect(step1.y, firstOrder1.u) annotation(
    Line(points = {{-59, 18}, {-48, 18}}, color = {0, 0, 127}));
  annotation(
    uses(Modelica(version = "3.2.2")),
    Diagram(coordinateSystem(extent = {{-100, -100}, {250, 100}})),
    Icon(coordinateSystem(extent = {{-100, -100}, {250, 100}})),
    version = "",
    __OpenModelica_commandLineOptions = "");
end Simulacao_modelo_malha_aberta;
